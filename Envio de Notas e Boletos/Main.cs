﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Envio_de_Notas_e_Boletos
{
    public partial class Main : Form
    {
        private string Login;
        private string Senha;
        private List<EmailArquivo> ArquivosEmail;
        private string[] ArquivosCobranca;
        private string[] ArquivosNotaCensanet;
        private string[] ArquivosNotaImatelecom;
        private string pastaCobranca;
        private string pastaNotaCensanet;
        private string pastaNotaImatelecom;
        private string Log;

        public Main()
        {
            InitializeComponent();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private bool EnviarEmailAnexo(string para, string assunto, string mensagem, List<string> anexos)
        {
            bool status = true;

            Char delimiter = '@';            
            string[] host = this.Login.Split(delimiter);
            
            if (!string.IsNullOrWhiteSpace(para) && !string.IsNullOrWhiteSpace(assunto))
            {
                SmtpClient smtp = new SmtpClient();
                MailMessage mail = new MailMessage();                

                foreach (string anexo in anexos)
                {                    
                    Attachment anexar = new Attachment(anexo);
                    mail.Attachments.Add(anexar);
                }

                smtp.Host = $"smtp.{host[1]}";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential(this.Login, this.Senha);

                // carrega o html de um template html existente e substitui o texto
                /*
                using (StreamReader objReader = new StreamReader("Recuperacao_senha.html"))
                {
                    // Lê todo o arquivo e o joga em uma variável
                    mail.Body = objReader.ReadToEnd();
                    strMail = strMail.Replace("#NOME", conta.Usuario);
                }*/


                // configurações para o envio do e-mail
                mail.Priority = MailPriority.High;                                
                mail.From = new MailAddress("j.rodolfopps@gmail.com");
                //mail.CC.Add(new MailAddress(""));
                mail.To.Add(new MailAddress(para));

                //mail.IsBodyHtml = true;
                mail.SubjectEncoding = Encoding.GetEncoding("utf-8");
                mail.BodyEncoding = Encoding.GetEncoding("utf-8");
                mail.Subject = assunto;
                mail.Body = mensagem;

                smtp.Send(mail);
            }
            else
            {
                status = false;
            }

            return status;
        }

        private void buttonEnviarEmail_Click(object sender, EventArgs e)
        {
            this.Log = "";
            this.Login = this.textBoxLogin.Text;
            this.Senha = this.textBoxSenha.Text;

            string para = "jrodolfo@censanet.com.br";
            string assunto = this.textBoxAssuntoEmail.Text;
            string mensagem = this.richTextBoxCorpoEmail.Text;
            
            bool temArquivosParaEnvio = true;

            if (String.IsNullOrWhiteSpace(assunto) || String.IsNullOrWhiteSpace(mensagem))
            {
                DialogResult resultadoEnvio = MessageBox.Show("Você deseja enviar o e-mail sem \"Assunto/Mensagem\"?",
                    "Atenção ao Enviar E-mail", MessageBoxButtons.YesNo);

                if (resultadoEnvio == DialogResult.Yes)
                {
                    if (String.IsNullOrWhiteSpace(assunto))
                        assunto = "(Sem Assunto)";

                    if (String.IsNullOrWhiteSpace(mensagem))
                        mensagem = "";
                }
                else
                {
                    temArquivosParaEnvio = false;
                    this.Log += "Por favor, informe um \"Assunto/Mensagem\" para o envio dos E-mails. \n\n";
                }
            }

            if (ArquivosEmail == null || ArquivosCobranca == null || ArquivosNotaCensanet == null || ArquivosNotaImatelecom == null
                || String.IsNullOrWhiteSpace(this.Login) || String.IsNullOrWhiteSpace(this.Senha))
            {
                temArquivosParaEnvio = false;

                if (ArquivosEmail == null)
                    this.Log += "Por favor, selecione o arquivo com os E-mails no padrão: Cesta; Nome; E-mail. \n";

                if (ArquivosCobranca == null)                
                    this.Log += "Por favor, selecione a pasta com às Cobranças. \n";

                if (ArquivosNotaCensanet == null)
                    this.Log += "Por favor, selecione a pasta com às Notas do Censanet. \n";

                if (ArquivosNotaImatelecom == null)
                    this.Log += "Por favor, selecione a pasta com às Notas da Imatelecom. \n";

                if (String.IsNullOrWhiteSpace(this.Login))
                    this.Log += "Por favor, informe o Login para o envio dos E-mails. \n";

                if (String.IsNullOrWhiteSpace(this.Senha))
                    this.Log += "Por favor, informe o Senha para o envio dos E-mails. \n";
                
                this.Log += "\n";
            }            

            if (temArquivosParaEnvio)
            {
                Char delimiter = '_';
                int countEnvio = 0;
                foreach (EmailArquivo dado in ArquivosEmail)
                {
                    var anexos = new List<string>();                    
                    bool teveCobranca = false;
                    bool teveNotaCensanet = false;
                    bool teveNotaImatelecom = false;

                    foreach (string arquivoCobranca in ArquivosCobranca)
                    {
                        string nomeArquivo = arquivoCobranca.Replace(this.pastaCobranca, "");
                        var cesta = Regex.Match(nomeArquivo, @"\d+").Value;
                        if (cesta == dado.Cesta)
                        {
                            teveCobranca = true;
                            anexos.Add(arquivoCobranca);
                        }
                    }

                    foreach (string arquivoNotaCensanet in ArquivosNotaCensanet)
                    {
                        string nomeArquivo = arquivoNotaCensanet.Replace(this.pastaCobranca, "");
                        var cesta = Regex.Match(nomeArquivo, @"\d+").Value;
                        if (cesta == dado.Cesta)
                        {
                            teveNotaCensanet = true;
                            anexos.Add(arquivoNotaCensanet);
                        }
                    }

                    foreach (string arquivoNotaImatelecom in ArquivosNotaImatelecom)
                    {
                        var caminho = arquivoNotaImatelecom.Split(new char[] { delimiter, delimiter });
                        var cesta = Regex.Match(caminho[caminho.Length - 1], @"\d+").Value;
                        if (cesta == dado.Cesta)
                        {
                            teveNotaImatelecom = true;
                            anexos.Add(arquivoNotaImatelecom);
                        }
                    }

                    if (anexos.Count > 0 && teveCobranca && teveNotaCensanet && teveNotaImatelecom)
                    {
                        if (this.EnviarEmailAnexo(para, assunto, mensagem, anexos))
                            this.Log += $"Cesta: {dado.Cesta} - E-mail enviado com sucesso. \n";
                        else
                            this.Log += $"Cesta: {dado.Cesta} - Erro ao enviar E-mail. \n";
                    }
                    else
                    {
                        this.Log += $"Cesta: {dado.Cesta} - Não foi encontrado todos os Anexos necessários para o Envio, por favor verificar se os mesmos estão disponíveis nos arquivos selecionados. \n";
                    }

                    countEnvio++;
                    if (countEnvio == 3)
                    {
                        break;
                    }
                }
            }

            var log = new Log(this.Log);
            log.ShowDialog();
        }

        private void buttonSelecionarEmail_Click(object sender, EventArgs e)
        {
            this.ArquivosEmail = new List<EmailArquivo>();

            OpenFileDialog openFileDialogEmail = new OpenFileDialog();
            openFileDialogEmail.Multiselect = false;
            openFileDialogEmail.Title = "Selecionar Arquivo de E-mail";            
            openFileDialogEmail.Filter = "Arquivo (*.CSV)|*.CSV";
            openFileDialogEmail.CheckFileExists = true;
            openFileDialogEmail.CheckPathExists = true;
            openFileDialogEmail.FilterIndex = 0;
            openFileDialogEmail.RestoreDirectory = true;
            openFileDialogEmail.ReadOnlyChecked = true;
            openFileDialogEmail.ShowReadOnly = true;

            if (openFileDialogEmail.ShowDialog() == DialogResult.OK)
            {
                string arquivoEmail = "";
                foreach (String arquivo in openFileDialogEmail.FileNames)
                {
                    arquivoEmail = arquivo;
                }

                try
                {
                    this.textBoxEmails.Text = arquivoEmail;
                    using (var reader = new StreamReader(arquivoEmail))
                    {
                        List<string> listA = new List<string>();
                        List<string> listB = new List<string>();
                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();
                            var values = line.Split(';');
                            
                            var dado = new EmailArquivo();
                            dado.Cesta = values[0];
                            dado.NomeCliente = values[1];
                            dado.Email = values[2];

                            ArquivosEmail.Add(dado);
                        }
                    }
                }
                catch (IOException a)
                {
                    MessageBox.Show(a.Message);
                }
            }
        }

        private void buttonSelecionarPastaCobrancas_Click(object sender, EventArgs e)
        {
            try {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        this.pastaCobranca = fbd.SelectedPath;
                        this.ArquivosCobranca = Directory.GetFiles(fbd.SelectedPath);
                        this.textBoxPastaCobrancas.Text = this.pastaCobranca;
                    }
                }               
            }
            catch (IOException a)
            {
                MessageBox.Show(a.Message);
            }
        }

        private void buttonSelecionarPastaNotas_Click(object sender, EventArgs e)
        {
            try
            {                
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        this.pastaNotaCensanet = fbd.SelectedPath;
                        this.ArquivosNotaCensanet = Directory.GetFiles(fbd.SelectedPath);
                        this.textBoxPastaNotasCensanet.Text = this.pastaNotaCensanet;
                    }
                }
            }
            catch (IOException a)
            {
                MessageBox.Show(a.Message);
            }
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void buttonSelecionarPastaNotasImatelecom_Click(object sender, EventArgs e)
        {
            try
            {
                using (var fbd = new FolderBrowserDialog())
                {
                    DialogResult result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                    {
                        this.pastaNotaImatelecom = fbd.SelectedPath;
                        this.ArquivosNotaImatelecom = Directory.GetFiles(fbd.SelectedPath);
                        this.textBoxPastaNotasImatelecom.Text = this.pastaNotaImatelecom;
                    }
                }
            }
            catch (IOException a)
            {
                MessageBox.Show(a.Message);
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(this.linkLabel1.Text);
        }
    }
}
