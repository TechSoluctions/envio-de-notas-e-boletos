﻿namespace Envio_de_Notas_e_Boletos
{
    partial class Main
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxEmails = new System.Windows.Forms.TextBox();
            this.buttonSelecionarEmail = new System.Windows.Forms.Button();
            this.buttonSelecionarPastaCobrancas = new System.Windows.Forms.Button();
            this.textBoxPastaCobrancas = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSelecionarPastaNotasCensanet = new System.Windows.Forms.Button();
            this.textBoxPastaNotasCensanet = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.richTextBoxCorpoEmail = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.textBoxSenha = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonEnviarEmail = new System.Windows.Forms.Button();
            this.textBoxAssuntoEmail = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label14 = new System.Windows.Forms.Label();
            this.buttonSelecionarPastaNotasImatelecom = new System.Windows.Forms.Button();
            this.textBoxPastaNotasImatelecom = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.radioButtonEnviarEmailDiferenteSim = new System.Windows.Forms.RadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.radioButtonEnviarEmailDiferenteNao = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 253);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lista de E-mails";
            // 
            // textBoxEmails
            // 
            this.textBoxEmails.Enabled = false;
            this.textBoxEmails.Location = new System.Drawing.Point(15, 269);
            this.textBoxEmails.Name = "textBoxEmails";
            this.textBoxEmails.Size = new System.Drawing.Size(456, 20);
            this.textBoxEmails.TabIndex = 1;
            // 
            // buttonSelecionarEmail
            // 
            this.buttonSelecionarEmail.Location = new System.Drawing.Point(477, 266);
            this.buttonSelecionarEmail.Name = "buttonSelecionarEmail";
            this.buttonSelecionarEmail.Size = new System.Drawing.Size(75, 23);
            this.buttonSelecionarEmail.TabIndex = 2;
            this.buttonSelecionarEmail.Text = "Selecionar";
            this.buttonSelecionarEmail.UseVisualStyleBackColor = true;
            this.buttonSelecionarEmail.Click += new System.EventHandler(this.buttonSelecionarEmail_Click);
            // 
            // buttonSelecionarPastaCobrancas
            // 
            this.buttonSelecionarPastaCobrancas.Location = new System.Drawing.Point(477, 305);
            this.buttonSelecionarPastaCobrancas.Name = "buttonSelecionarPastaCobrancas";
            this.buttonSelecionarPastaCobrancas.Size = new System.Drawing.Size(75, 23);
            this.buttonSelecionarPastaCobrancas.TabIndex = 5;
            this.buttonSelecionarPastaCobrancas.Text = "Selecionar";
            this.buttonSelecionarPastaCobrancas.UseVisualStyleBackColor = true;
            this.buttonSelecionarPastaCobrancas.Click += new System.EventHandler(this.buttonSelecionarPastaCobrancas_Click);
            // 
            // textBoxPastaCobrancas
            // 
            this.textBoxPastaCobrancas.Enabled = false;
            this.textBoxPastaCobrancas.Location = new System.Drawing.Point(15, 308);
            this.textBoxPastaCobrancas.Name = "textBoxPastaCobrancas";
            this.textBoxPastaCobrancas.Size = new System.Drawing.Size(456, 20);
            this.textBoxPastaCobrancas.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 292);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Pasta com as Cobranças";
            // 
            // buttonSelecionarPastaNotasCensanet
            // 
            this.buttonSelecionarPastaNotasCensanet.Location = new System.Drawing.Point(206, 345);
            this.buttonSelecionarPastaNotasCensanet.Name = "buttonSelecionarPastaNotasCensanet";
            this.buttonSelecionarPastaNotasCensanet.Size = new System.Drawing.Size(75, 23);
            this.buttonSelecionarPastaNotasCensanet.TabIndex = 8;
            this.buttonSelecionarPastaNotasCensanet.Text = "Selecionar";
            this.buttonSelecionarPastaNotasCensanet.UseVisualStyleBackColor = true;
            this.buttonSelecionarPastaNotasCensanet.Click += new System.EventHandler(this.buttonSelecionarPastaNotas_Click);
            // 
            // textBoxPastaNotasCensanet
            // 
            this.textBoxPastaNotasCensanet.Enabled = false;
            this.textBoxPastaNotasCensanet.Location = new System.Drawing.Point(15, 347);
            this.textBoxPastaNotasCensanet.Name = "textBoxPastaNotasCensanet";
            this.textBoxPastaNotasCensanet.Size = new System.Drawing.Size(185, 20);
            this.textBoxPastaNotasCensanet.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 331);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Pasta com as Notas do Censanet";
            // 
            // richTextBoxCorpoEmail
            // 
            this.richTextBoxCorpoEmail.Location = new System.Drawing.Point(15, 425);
            this.richTextBoxCorpoEmail.Name = "richTextBoxCorpoEmail";
            this.richTextBoxCorpoEmail.Size = new System.Drawing.Size(537, 162);
            this.richTextBoxCorpoEmail.TabIndex = 10;
            this.richTextBoxCorpoEmail.Text = "";
            this.richTextBoxCorpoEmail.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 409);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Corpo do E-mail";
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Location = new System.Drawing.Point(340, 610);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(212, 20);
            this.textBoxLogin.TabIndex = 12;
            // 
            // textBoxSenha
            // 
            this.textBoxSenha.Location = new System.Drawing.Point(340, 636);
            this.textBoxSenha.Name = "textBoxSenha";
            this.textBoxSenha.PasswordChar = '*';
            this.textBoxSenha.Size = new System.Drawing.Size(109, 20);
            this.textBoxSenha.TabIndex = 13;
            this.textBoxSenha.UseSystemPasswordChar = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label5.Location = new System.Drawing.Point(296, 613);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "E-mail:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(293, 639);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Senha:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonEnviarEmail
            // 
            this.buttonEnviarEmail.Location = new System.Drawing.Point(455, 634);
            this.buttonEnviarEmail.Name = "buttonEnviarEmail";
            this.buttonEnviarEmail.Size = new System.Drawing.Size(97, 23);
            this.buttonEnviarEmail.TabIndex = 16;
            this.buttonEnviarEmail.Text = "Enviar E-mail";
            this.buttonEnviarEmail.UseVisualStyleBackColor = true;
            this.buttonEnviarEmail.Click += new System.EventHandler(this.buttonEnviarEmail_Click);
            // 
            // textBoxAssuntoEmail
            // 
            this.textBoxAssuntoEmail.Location = new System.Drawing.Point(15, 386);
            this.textBoxAssuntoEmail.Name = "textBoxAssuntoEmail";
            this.textBoxAssuntoEmail.Size = new System.Drawing.Size(537, 20);
            this.textBoxAssuntoEmail.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 370);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Assunto do E-mail";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = global::Envio_de_Notas_e_Boletos.Properties.Resources.censanet;
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(214, 218);
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(252, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(289, 26);
            this.label8.TabIndex = 20;
            this.label8.Text = "Envio de Cobranças e Notas";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(265, 162);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 17);
            this.label9.TabIndex = 21;
            this.label9.Text = "Versão:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(254, 139);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 17);
            this.label10.TabIndex = 22;
            this.label10.Text = "Empresa:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(286, 184);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 17);
            this.label11.TabIndex = 23;
            this.label11.Text = "Site:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(328, 139);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 17);
            this.label12.TabIndex = 24;
            this.label12.Text = "Censanet";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(328, 162);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 17);
            this.label13.TabIndex = 25;
            this.label13.Text = "1.0.0";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(328, 188);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(149, 13);
            this.linkLabel1.TabIndex = 26;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "https://www.censanet.com.br";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(254, 57);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(304, 68);
            this.label14.TabIndex = 27;
            this.label14.Text = "Esta aplicação tem como principal objetivo \r\nenviar as Cobranças do Censanet e No" +
    "tas \r\ndo Censanet e Imatelecom para as Pessoas \r\nJurídicas que compartilham as m" +
    "esmas cestas.";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // buttonSelecionarPastaNotasImatelecom
            // 
            this.buttonSelecionarPastaNotasImatelecom.Location = new System.Drawing.Point(477, 344);
            this.buttonSelecionarPastaNotasImatelecom.Name = "buttonSelecionarPastaNotasImatelecom";
            this.buttonSelecionarPastaNotasImatelecom.Size = new System.Drawing.Size(75, 23);
            this.buttonSelecionarPastaNotasImatelecom.TabIndex = 30;
            this.buttonSelecionarPastaNotasImatelecom.Text = "Selecionar";
            this.buttonSelecionarPastaNotasImatelecom.UseVisualStyleBackColor = true;
            this.buttonSelecionarPastaNotasImatelecom.Click += new System.EventHandler(this.buttonSelecionarPastaNotasImatelecom_Click);
            // 
            // textBoxPastaNotasImatelecom
            // 
            this.textBoxPastaNotasImatelecom.Enabled = false;
            this.textBoxPastaNotasImatelecom.Location = new System.Drawing.Point(287, 347);
            this.textBoxPastaNotasImatelecom.Name = "textBoxPastaNotasImatelecom";
            this.textBoxPastaNotasImatelecom.Size = new System.Drawing.Size(184, 20);
            this.textBoxPastaNotasImatelecom.TabIndex = 29;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(284, 331);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(174, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "Pasta com as Notas da Imatelecom";
            // 
            // radioButtonEnviarEmailDiferenteSim
            // 
            this.radioButtonEnviarEmailDiferenteSim.AutoSize = true;
            this.radioButtonEnviarEmailDiferenteSim.Location = new System.Drawing.Point(18, 637);
            this.radioButtonEnviarEmailDiferenteSim.Name = "radioButtonEnviarEmailDiferenteSim";
            this.radioButtonEnviarEmailDiferenteSim.Size = new System.Drawing.Size(42, 17);
            this.radioButtonEnviarEmailDiferenteSim.TabIndex = 31;
            this.radioButtonEnviarEmailDiferenteSim.Text = "Sim";
            this.radioButtonEnviarEmailDiferenteSim.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 613);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(180, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Enviar E-mail diferente do censanet?";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radioButtonEnviarEmailDiferenteNao
            // 
            this.radioButtonEnviarEmailDiferenteNao.AutoSize = true;
            this.radioButtonEnviarEmailDiferenteNao.Checked = true;
            this.radioButtonEnviarEmailDiferenteNao.Location = new System.Drawing.Point(66, 637);
            this.radioButtonEnviarEmailDiferenteNao.Name = "radioButtonEnviarEmailDiferenteNao";
            this.radioButtonEnviarEmailDiferenteNao.Size = new System.Drawing.Size(166, 17);
            this.radioButtonEnviarEmailDiferenteNao.TabIndex = 33;
            this.radioButtonEnviarEmailDiferenteNao.TabStop = true;
            this.radioButtonEnviarEmailDiferenteNao.Text = "Não. Usar e-mail do Censanet";
            this.radioButtonEnviarEmailDiferenteNao.UseVisualStyleBackColor = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(567, 681);
            this.Controls.Add(this.radioButtonEnviarEmailDiferenteNao);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.radioButtonEnviarEmailDiferenteSim);
            this.Controls.Add(this.buttonSelecionarPastaNotasImatelecom);
            this.Controls.Add(this.textBoxPastaNotasImatelecom);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBoxAssuntoEmail);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.buttonEnviarEmail);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxSenha);
            this.Controls.Add(this.textBoxLogin);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.richTextBoxCorpoEmail);
            this.Controls.Add(this.buttonSelecionarPastaNotasCensanet);
            this.Controls.Add(this.textBoxPastaNotasCensanet);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonSelecionarPastaCobrancas);
            this.Controls.Add(this.textBoxPastaCobrancas);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonSelecionarEmail);
            this.Controls.Add(this.textBoxEmails);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "Censanet - Envio de Cobranças e Notas por E-mail";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxEmails;
        private System.Windows.Forms.Button buttonSelecionarEmail;
        private System.Windows.Forms.Button buttonSelecionarPastaCobrancas;
        private System.Windows.Forms.TextBox textBoxPastaCobrancas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonSelecionarPastaNotasCensanet;
        private System.Windows.Forms.TextBox textBoxPastaNotasCensanet;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox richTextBoxCorpoEmail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.TextBox textBoxSenha;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonEnviarEmail;
        private System.Windows.Forms.TextBox textBoxAssuntoEmail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button buttonSelecionarPastaNotasImatelecom;
        private System.Windows.Forms.TextBox textBoxPastaNotasImatelecom;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RadioButton radioButtonEnviarEmailDiferenteSim;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RadioButton radioButtonEnviarEmailDiferenteNao;
    }
}

