﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Envio_de_Notas_e_Boletos
{
    public class EmailArquivo
    {
        public string Cesta { get; set; }
        public string NomeCliente { get; set; }
        public string Email { get; set; }

        override
            public String ToString()
        {
            return $"{Cesta} - {NomeCliente}  - {Email}";
        }
    }
}
