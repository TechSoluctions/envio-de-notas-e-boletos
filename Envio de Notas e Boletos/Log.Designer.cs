﻿namespace Envio_de_Notas_e_Boletos
{
    partial class Log
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBoxLogEnvioEmail = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // richTextBoxLogEnvioEmail
            // 
            this.richTextBoxLogEnvioEmail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxLogEnvioEmail.Location = new System.Drawing.Point(3, 3);
            this.richTextBoxLogEnvioEmail.Margin = new System.Windows.Forms.Padding(0);
            this.richTextBoxLogEnvioEmail.Name = "richTextBoxLogEnvioEmail";
            this.richTextBoxLogEnvioEmail.ReadOnly = true;
            this.richTextBoxLogEnvioEmail.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richTextBoxLogEnvioEmail.Size = new System.Drawing.Size(678, 305);
            this.richTextBoxLogEnvioEmail.TabIndex = 0;
            this.richTextBoxLogEnvioEmail.Text = "";
            // 
            // Log
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 311);
            this.Controls.Add(this.richTextBoxLogEnvioEmail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "Log";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Log - E-mails enviados";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxLogEnvioEmail;
    }
}